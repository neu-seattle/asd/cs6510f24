
= Course Description
:description: CS6510 course description

Welcome to Advanced Software Development (ASD) for Sprint 2025. The class this
semester will be slightly different from previous years.

The class is designed as a project based class with students working in teams.
The project along with the material covered will be focused around DevOps,
specifically building a Continuous Integration/Continuous Deployment (CI/CD)
system.

On top of each team using Software Development Processes to manage themselves
and the software they will produce, the class will also cover, to a certain depth,
Git integration, GitOps, virtualization and containerization (Containers and
Kubernetes), REST APIs, cluster management and deployment.
