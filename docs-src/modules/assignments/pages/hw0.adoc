= Class setup

This is not really an assignment but more of a list of tools and/or
registration(s) that you must perform that are required for our class.

== Discord

We will be using https://discord.com/[Discord^] for Q/A and online discussions. The class staff has
created a server on Discord, use this link to join the server.

https://discord.gg/DNuaDfge[Discord Invite^]


Feel free to use Discord's
https://support.discord.com/hc/en-us/categories/115000217151[features] to learn
more and use it with your team.


== Git

Make sure you have https://git-scm.com/downloads[Git installed^] on your machine.
At a minimum you should have access to the `git` command on your machine's
terminal.

There are many Git tools that provide better integration with repo hosting tools (like
GitHub and GitLab)/IDEs/OSes.  You are free to use what you prefer, however, the class will
be using the common denominator, the `git` command, for instructional material and live
demos.


== GitHub Login

We will be using GitHub Classroom to organize individual and team assignments. In order to do so we will need
your GitHub login.

[IMPORTANT]
****
We are using the **public** GitHub installation at https://github.com/. Please
provide your login **for the public GitHub instance**.

Northeastern has it's own GitHub Enterprise installation that you may have used in other classes.
**That is not what we are using in this class.**
****

Fill in out our https://forms.gle/haYVaKASqLKSogmw7[Student Information Google Form^]. You will be asked to provide

. Your full name. Please use the same spelling of your name as the one used in the Registrar's system.
. Your https://github.com/login user name.
. Your Discord user name.
