= Rubric
:icons+: font


== Backlog

Each team will maintain its own backlog using GitHub Issues. The team
will be responsible for managing the backlog, creating, analyzing, and
assigning tasks.

=== Backlog requirements

For each issue that is part of a weekly sprint, the team is **required** to add

. Estimates for the issue. The team is free to use the estimate system
that they prefer, e.g., points, t-shirt sizes etc.
. Assignee. The task must be assigned to **one** team member.
. Description. The issue **must** have a description that explains
what the task is about.

== Weekly Progress Reports

Each team will generate a weekly report. The weekly report captures
the teams progress each week as well as any other decisions and/or
processes changes made during the week.

The team repository will contain a template in markdown that you can use to
fill in each week.

=== Requirements

A team's weekly report is a truthful account of the state of the
project that can be verified by the data in the team's repository.

. An issue is considered **complete** or **done** when it has
generated the expected output in `main`.
.. For coding issues the expected output should be a Git commit from a
PR into the `main` branch
.. For a design issues the expected output should be a document
(markdown file at a minimum) with the outcome of the design task
. PRs **must** link to a an Issue.

Filling out the weekly report template file the teams **must**

. include links to each issue referred in the weekly report
. PRs **must** link to the issue they are addressing. See
https://docs.github.com/en/issues/tracking-your-work-with-issues/using-issues/linking-a-pull-request-to-an-issue[GitHub
documentation] on how to link PRs and Issue.
. For incomplete tasks, create a **DRAFT PR** and link that DRAFT PR
in your weekly report. Make sure your DRAFT PR links to the issue it
is trying to address.


== Interim Presentations

During the semester, the course staff will reach out to each team in
order to prepare interim presentation(s). These presentation will take
place during normal lecture hours.

For your interim presentations you will be asked to provide an update
of the team's progress including the parts of the design that the team
has been focusing the past week.

During the interim presentation you should be able to

* provide a short demo (or show and tell) for a part of the
  application
* discuss design decisions made during development
* walk through the implementation of a component. You will be given
  the component/code section to walk through in advance by the course
  staff
* hold a Q&A session with the rest of the class


== Codewalks

=== Codewalk 1: Configuration file Verification

Each team will have 30 minutes for their codewalk. Allow for 5 minutes
of Q&A from the audience.

For the first codewalk we will be focusing on the CI/CD configuration
file and its requirements.

Your codewalk must include the following

. High level overview of your whole system.
.. What are the main components? What is each component's
responsibility?
.. What is the flow of communication and information between your
components?
. What are the features that are implemented for the Configuration
file?
.. Demo your code showing the features that have been implemented.
.. If there are parts that are mocked out for the demo, mention them.
. Walk through the code that was part of the demo.
.. Did you use any well known algorithms?
.. How did you organize the code and why?
.. What testing did you perform?
. What is the one thing that went well this week?
.. What do you plan to do to keep this thing going well?
.. Can other parts of your process benefit from what happened here?
. What is the one thing that did not go well this week?
.. What do you plan to do next week to avoid or address the reasons
behind the one thing that did not go well?
. Open the floor for questions.


Be prepared to answer questions on your design and code including.

. Providing reasons for your choices in design/code
. Giving a list of possible changes for a proposed extension/addition
to a requirement you have already implemented.
. Talking about pros and cons between 2 design decisions, e.g., using
a library for verification vs using a REST API call to a service.


=== Codewalk 2


[IMPORTANT]
.Student provided Feedback
====

This week each student will provide feedback to each team
that presented (excluding your own team). We will collect **anonymous**
feedback from each student using
https://forms.gle/yvtsLsaLt58qkMWK9[this Feedback Form].

**DO NOT FILL THE FORM UNTIL ALL TEAMS HAVE PRESENTED**

====

Each team will have 30 minutes for their codewalk. Allow for 5 minutes
of Q&A from the audience.

For our second codewalk each team will present the work that they have
completed this past week.

Your codewalk should include

. What the team decided to focus on at the beginning of the week?
.. Give a high level description here. You can show the task titles
and spend about a minute on what the task is about.
. What did the team manage to complete?
.. Which tasks got done?
.. Which tasks are pending?
.. Which tasks got dropped?
.. Which new tasks were created during the week?
. Demo your work
.. Focus your demo on the use case and the features added/updated this
week.
.. If the team completed design tasks, show the result of the design.
. What went well this week?
. What did not go well this week?
. What do you plan to focus on next week and why?


=== Codewalk 3 (Demos and Q&A)

[IMPORTANT]
.Student provided Feedback
====

This week each student will provide feedback to each team
that presented (excluding your own team). We will collect **anonymous**
feedback from each student using
https://forms.gle/5Yc3YRc1uqnt6zu68[this Feedback Form].

**DO NOT FILL THE FORM UNTIL ALL TEAMS HAVE PRESENTED**

====


Each team will have 15 minutes to present their demo. Keep it short
keep it organized.

. Tell us which features you are about to Demo.
.. What was implemented this week. Do not go into details of the code,
describe what was implemented in terms of what a user can do today
that they could not do last week.
.. If for the demo certain parts of the system are mocked out
(unimplemented or half implemented) tells the audience which parts.
. Q&A

[NOTE]
.Organize your presentation and prepare what you are going to run
====

We need to keep demos within the time frame. Do everything you can to
prepare your demo so that we do not spend time setting up/preparing.


. Have **one laptop and only one laptop** setup and ready to go.
**Do not change laptops during the presentation**.
+
. Any of your systems component that you need for the demo **must be
ready before you start your demo**. Any processes, services, DBs etc.,
must be setup and already running **before** you are called up to
present.
. If you are executing any commands for the demo, have them ready in
scripts **beforehand**. All you should be doing for the demo is
calling a script, e.g., `demo1`, `demo2` etc. **Do not spend time
trying to find the command, copy paste the command, fix the command or type it out by hand**.
. **Have your IDE open, ready and minimized**. You are going to be asked
questions about the code. Make sure you can flip to the IDE quickly
and not have to wait to load it.
. **One person drives the demo**. Pick one presenter for the
demo. They are going to be in charge to walk us through the
demo. Questions can be answered by all members of the team not just
the presenter. The presenter is only there to drive the demo.

====
