
.PHONY: all deps html
FILES_ADOC = $(shell find . -name '*.adoc')
FILES_SUPPLIMENT = $(shell find supplemental-ui/)
FILES_YML = $(shell find . -name 'antora.yml' -o -name 'antora-playbook.yml' -o -name 'local-antora-playbook.yml')

all: deps html

node_modules/.bin/antora:
	npm i



deps: node_modules/.bin/antora $(FILES_ADOC) $(FILES_SUPPLIMENT) $(FILES_YML)

html: deps
	rm -rf build;\
	npx antora --fetch local-antora-playbook.yml --stacktrace
